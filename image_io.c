#include "image_io.h"

double **bmp_in(FILE *bmpfile, int32_t *y, int32_t *x)
{
	int32_t	iy, ix, ic;
	int32_t offset;
	double	**image;
	uint8_t	zerobytes, val;
	
	if (fread_le_short(bmpfile) != 19778)
	{
		fprintf(stderr, "This file is not in BMP format\n");
		exit(EXIT_FAILURE);
	}

	fseek(bmpfile, 8, SEEK_CUR);
	offset = fread_le_word(bmpfile) - 54;
	fseek(bmpfile, 4, SEEK_CUR);
	*x = fread_le_word(bmpfile);
	*y = fread_le_word(bmpfile);
	fseek(bmpfile, 2, SEEK_CUR);

	if (fread_le_short(bmpfile) != 24)
	{
		fprintf(stderr, "Wrong BMP format, BMP images must be in 24-bit colour\n");
		exit(EXIT_FAILURE);
	}

	fseek(bmpfile, 24+offset, SEEK_CUR);

	image = malloc (*y * sizeof(double));
	for (iy=0; iy<*y; iy++)
		image[iy]=calloc (*x, sizeof(double));


	zerobytes = 4 - ((*x*3) & 3);
	if (zerobytes==4)
		zerobytes = 0;

	for (iy=*y-1; iy!=-1; iy--)
	{
		for (ix=0; ix<*x; ix++)
		{
			for (ic=2;ic!=-1;ic--)
			{
				fread(&val, 1, 1, bmpfile);
				image[iy][ix] += (double) val * (1.0/(255.0 * 3.0));
			}
		}

		fseek(bmpfile, zerobytes, SEEK_CUR);
	}

	fclose(bmpfile);
	return image;
}

void bmp_out(FILE *bmpfile, double **image, int32_t y, int32_t x)
{
	int32_t	i, iy, ix, ic;
	int32_t	filesize, imagesize;
	uint8_t	zerobytes, val, zero=0;
	double	vald;

	zerobytes = 4 - ((x*3) & 3);
	if (zerobytes==4)
		zerobytes = 0;

	filesize = 56 + ((x*3)+zerobytes) * y;
	imagesize = 2 + ((x*3)+zerobytes) * y;

	fwrite_le_short(19778, bmpfile);
	fwrite_le_word(filesize, bmpfile);
	fwrite_le_word(0, bmpfile);
	fwrite_le_word(54, bmpfile);
	fwrite_le_word(40, bmpfile);
	fwrite_le_word(x, bmpfile);
	fwrite_le_word(y, bmpfile);
	fwrite_le_short(1, bmpfile);
	fwrite_le_word(24, bmpfile);
	fwrite_le_short(0, bmpfile);
	fwrite_le_word(imagesize, bmpfile);
	fwrite_le_word(2834, bmpfile);
	fwrite_le_word(2834, bmpfile);
	fwrite_le_word(0, bmpfile);
	fwrite_le_word(0, bmpfile);

	for (iy=y-1; iy!=-1; iy--)
	{
		for (ix=0; ix<x; ix++)
		{
			vald = image[iy][ix] * 255.0;

			if (vald > 255.0)
				vald = 255.0;

			if (vald < 0.0)
				vald = 0.0;

			val = vald;

			for (ic=2; ic!=-1; ic--)
				fwrite(&val, 1, 1, bmpfile);
		}
		for (i=0; i<zerobytes; i++)
			fwrite(&zero, 1, 1, bmpfile);
	}

	fwrite_le_short(0, bmpfile);

	fclose(bmpfile);

	printf("Image size : %dx%d\n", x, y);
}
