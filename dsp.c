#include "dsp.h"

void fft(double *in, double *out, int32_t N, uint8_t method)
{
	fftw_plan p = fftw_plan_r2r_1d(N, in, out, method, FFTW_ESTIMATE);
	fftw_execute(p);
	fftw_destroy_plan(p);
}

void normi(double **s, int32_t xs, int32_t ys, double ratio)
{
	int32_t ix, iy, maxx, maxy;
	double max;

	max=0;
	for (iy=0; iy<ys; iy++)
		for (ix=0; ix<xs; ix++)
			if (fabs(s[iy][ix])>max)
			{
				max=fabs(s[iy][ix]);
				maxx=ix;
				maxy=iy;
			}

	if (max!=0.0)
	{
		max /= ratio;
		max = 1.0/max;
	}
	else
		max = 0.0;

	for (iy=0; iy<ys; iy++)
		for (ix=0; ix<xs; ix++)
			s[iy][ix]*=max;

}

double log_pos(double x, double min, double max)
{
	if (LOGBASE==1.0)
		return x*(max-min) + min;
	else
		return (max-min) * (min * pow(LOGBASE, x * (log(max)-log(min))/log(2.0)) - min) / (min * pow(LOGBASE, (log(max)-log(min))/log(2.0)) - min) + min;
}

double log_pos_inv(double x, double min, double max)
{
	if (LOGBASE==1.0)
		return (x - min)/(max-min);
	else
		return log(((x-min) * (min * pow(LOGBASE, (log(max) - log(min))/log(2.0)) - min) / (max-min) + min) / log(LOGBASE)) * log(2.0) / (log(max) - log(min));
}

double *freqarray(double basefreq, int32_t bands, double bandsperoctave)
{
	int32_t i;
	double *freq, maxfreq;

	freq=malloc (sizeof(double) * bands);

	if (LOGBASE==1.0)
		maxfreq = bandsperoctave;
	else
		maxfreq = basefreq * pow(LOGBASE, ((double) (bands-1)/ bandsperoctave));

	for (i=0;i<bands;i++)
	{
		freq[i] = log_pos((double) i/(double) (bands-1), basefreq, maxfreq);
	}
	if (log_pos((double) bands / (double) (bands-1), basefreq, maxfreq)>0.5)
		printf("Warning: Upper frequency limit above Nyquist frequency\n");

	return freq;
}

double *blackman_downsampling(double *in, int32_t Mi, int32_t Mo)
{
	int32_t i, j;
	double *out;
	double pos_in;
	double x;
	double ratio;
	double ratio_i;
	double coef;
	double coef_sum;

	ratio = (double) Mi/Mo;
	ratio_i = 1.0/ratio;

	out = calloc (Mo, sizeof(double));

	for (i=0; i<Mo; i++)
	{
		pos_in = (double) i * ratio;

		coef_sum = 0;

		for (j=roundup(pos_in - ratio); j<=pos_in + ratio; j++)
		{
			if (j>=0 && j<Mi)
			{
				x = j - pos_in + ratio;
				coef = 0.42 - 0.5*cos(pi * x * ratio_i) + 0.08*cos(2*pi * x * ratio_i);
				coef_sum += coef;
				out[i] += in[j] * coef;
			}
		}

		out[i] /= coef_sum;
	}

	return out;
}

double *bmsq_lut(int32_t size)
{
	int32_t i;
	double coef;
	double bar = pi * (3.0 / (double) size) * (1.0/1.5);
	double foo;

	double f1 = -0.6595044010905501;
	double f2 =  0.1601741366715479;
	double f4 = -0.0010709178680006;
	double f5 =  0.0001450093579222;
	double f7 =  0.0001008528049040;
	double f8 =  0.0000653092892874;
	double f10 = 0.0000293385615146;
	double f11 = 0.0000205351559060;
	double f13 = 0.0000108567682890;
	double f14 = 0.0000081549460136;
	double f16 = 0.0000048519309366;
	double f17 = 0.0000038284344102;
	double f19 = 0.0000024753630724;

	size++;

	double *lut = calloc (size, sizeof(double));

	for (i=0; i<size; i++)
	{
		foo = (double) i * bar;
		coef = 0;

		coef += cos(       foo) * f1  - f1;
		coef += cos( 2.0 * foo) * f2  - f2;
		coef += cos( 4.0 * foo) * f4  - f4;
		coef += cos( 5.0 * foo) * f5  - f5;
		coef += cos( 7.0 * foo) * f7  - f7;
		coef += cos( 8.0 * foo) * f8  - f8;
		coef += cos(10.0 * foo) * f10 - f10;
		coef += cos(11.0 * foo) * f11 - f11;
		coef += cos(13.0 * foo) * f13 - f13;
		coef += cos(14.0 * foo) * f14 - f14;
		coef += cos(16.0 * foo) * f16 - f16;
		coef += cos(17.0 * foo) * f17 - f17;
		coef += cos(19.0 * foo) * f19 - f19;

		lut[i] = coef;
	}

	return lut;
}

void blackman_square_interpolation(double *in, double *out, int32_t Mi, int32_t Mo, double *lut, int32_t lut_size)
{
	int32_t i, j;
	double pos_in;
	double x;
	double ratio;
	double ratio_i;
	double coef;
	double pos_lut;
	int32_t pos_luti;
	double mod_pos;
	double y0, y1;
	double foo = (double) lut_size / 3.0;
	int32_t j_start, j_stop;

	ratio = (double) Mi/Mo;
	ratio_i = 1.0/ratio;

	for (i=0; i<Mo; i++)
	{
		pos_in = (double) i * ratio;

		j_stop = pos_in + 1.5;

		j_start = j_stop - 2;
		if (j_start<0)
			j_start=0;

		if (j_stop >= Mi)
			j_stop = Mi - 1;

		for (j=j_start; j<=j_stop; j++)
		{
			x = j - pos_in + 1.5;
			pos_lut = x * foo;
			pos_luti = (int32_t) pos_lut;

			mod_pos = fmod(pos_lut, 1.0);

			y0 = lut[pos_luti];
			y1 = lut[pos_luti + 1];
			coef = y0 + mod_pos * (y1 - y0);

			out[i] += in[j] * coef;
		}
	}
}

double **anal(double *s, int32_t samplecount, int32_t samplerate, int32_t *Xsize, int32_t bands, double bpo, double pixpersec, double basefreq)
{
	int32_t i, ib, Mb, Mc, Md, Fa, Fd;
	double **out, *h, *freq, *t, coef, La, Ld, Li, maxfreq;
	
	freq = freqarray(basefreq, bands, bpo);

	if (LOGBASE==1.0)
		maxfreq = bpo;
	else
		maxfreq = basefreq * pow(LOGBASE, ((double) (bands-1)/ bpo));

	*Xsize = samplecount * pixpersec;
	if (fmod((double) samplecount * pixpersec, 1.0) != 0.0)
		(*Xsize)++;
	printf("Image size : %dx%d\n", *Xsize, bands);
	out = malloc (bands * sizeof(double *));

	clocka=gettime();

	if (LOGBASE==1.0)
		Mb = samplecount - 1 + (int32_t) roundoff(5.0/ freq[1]-freq[0]);
	else
		Mb = samplecount - 1 + (int32_t) roundoff(2.0*5.0/((freq[0] * pow(LOGBASE, -1.0/(bpo))) * (1.0 - pow(LOGBASE, -1.0 / bpo))));
	if (Mb % 2 == 1)
		Mb++;

	Mb = roundoff((double) nextsprime((int32_t) roundoff(Mb * pixpersec)) / pixpersec);

	Md = roundoff(Mb * pixpersec);

	s = realloc (s, Mb * sizeof(double));
	memset(&s[samplecount], 0, (Mb-samplecount) * sizeof(double));
	fft(s, s, Mb, 0);

	for (ib=0; ib<bands; ib++)
	{
		Fa = roundoff(log_pos((double) (ib-1)/(double) (bands-1), basefreq, maxfreq) * Mb);
		Fd = roundoff(log_pos((double) (ib+1)/(double) (bands-1), basefreq, maxfreq) * Mb);
		La = log_pos_inv((double) Fa / (double) Mb, basefreq, maxfreq);
		Ld = log_pos_inv((double) Fd / (double) Mb, basefreq, maxfreq);

		if (Fd > Mb/2)
			Fd = Mb/2;

		if (Fa<1)
			Fa=1;

		Mc = (Fd-Fa)*2 + 1;

		if (Md>Mc)
			Mc = Md;

		if (Md<Mc)
			Mc = nextsprime(Mc);

		printf("%4d/%d (FFT size: %6d)   %.2f Hz - %.2f Hz\r", ib+1, bands, Mc, (double) Fa*samplerate/Mb, (double) Fd*samplerate/Mb);

		out[bands-ib-1] = calloc(Mc, sizeof(double));

		for (i=0; i<Fd-Fa; i++)
		{
			Li = log_pos_inv((double) (i+Fa) / (double) Mb, basefreq, maxfreq);
			Li = (Li-La)/(Ld-La);
			coef = 0.5 - 0.5*cos(2.0*pi*Li);
			out[bands-ib-1][i+1] = s[i+1+Fa] * coef;
			out[bands-ib-1][Mc-1-i] = s[Mb-Fa-1-i] * coef;
		}

		h = calloc(Mc, sizeof(double));

		for (i=0; i<Fd-Fa; i++)
		{
			h[i+1] = out[bands-ib-1][Mc-1-i];
			h[Mc-1-i] = -out[bands-ib-1][i+1];
		}

		fft(out[bands-ib-1], out[bands-ib-1], Mc, 1);
		fft(h, h, Mc, 1);

		for (i=0; i<Mc; i++)
			out[bands-ib-1][i] = sqrt(out[bands-ib-1][i]*out[bands-ib-1][i] + h[i]*h[i]);

		free(h);

		if (Mc < Md)
			out[bands-ib-1] = realloc(out[bands-ib-1], Md * sizeof(double));

		if (Mc > Md)
		{
			t = out[bands-ib-1];

			out[bands-ib-1] = blackman_downsampling(out[bands-ib-1], Mc, Md);

			free(t);
		}

		out[bands-ib-1] = realloc(out[bands-ib-1], *Xsize * sizeof(double));
	}

	printf("\n");

	normi(out, *Xsize, bands, 1.0);
	return out;
}

double *wsinc_max(int32_t length, double bw)
{
	int32_t i;
	int32_t bwl;
	double tbw;
	double *h;
	double x;
	double coef;

	tbw = bw * (double) (length-1);
	bwl = roundup(tbw);
	h = calloc (length, sizeof(double));

	for (i=1; i<length; i++)
		h[i] = 1.0;

	for (i=0; i<bwl; i++)
	{
		x = (double) i / tbw;
		coef = 0.42*x - (0.5/(2.0*pi))*sin(2.0*pi*x) + (0.08/(4.0*pi))*sin(4.0*pi*x);
		coef *= 1.0/0.42;
		h[i+1] = coef;
		h[length-1-i] = coef;
	}


	return h;
}

double *synt_sine(double **d, int32_t Xsize, int32_t bands, int32_t *samplecount, int32_t samplerate, double basefreq, double pixpersec, double bpo)
{
	double *s, *freq, *filter, *sband, sine[4], rphase;
	int32_t i, ib;
	int32_t Fc, Bc, Mh, Mn, sbsize;

	freq = freqarray(basefreq, bands, bpo);

	clocka=gettime();

	sbsize = nextsprime(Xsize * 2);
	
	*samplecount = roundoff(Xsize/pixpersec);
	printf("Sound duration : %.3f s\n", (double) *samplecount/samplerate);
	*samplecount = roundoff(0.5*sbsize/pixpersec);
	
	s = calloc(*samplecount, sizeof(double));
	sband = malloc (sbsize * sizeof(double));

	Bc = roundoff(0.25 * (double) sbsize);

	Mh = (sbsize + 1) >> 1;
	Mn = (*samplecount + 1) >> 1;

	filter = wsinc_max(Mh, 1.0/TRANSITION_BW_SYNT);

	for (ib=0; ib<bands; ib++)
	{
		memset(sband, 0, sbsize * sizeof(double));

		rphase = dblrand() * pi;

		for (i=0; i<4; i++)
			sine[i]=cos(i*2.0*pi*0.25 + rphase);

		for (i=0; i<Xsize; i++)
		{
			if ((i & 1) == 0)
			{
				sband[i<<1] = d[bands-ib-1][i] * sine[0];
				sband[(i<<1) + 1] = d[bands-ib-1][i] * sine[1];
			}
			else
			{
				sband[i<<1] = d[bands-ib-1][i] * sine[2];
				sband[(i<<1) + 1] = d[bands-ib-1][i] * sine[3];
			}			
		}

		fft(sband, sband, sbsize, 0);
		Fc = roundoff(freq[ib] * *samplecount);

		printf("%4d/%d   %.2f Hz\r", ib+1, bands, (double) Fc*samplerate / *samplecount);

		for (i=1; i<Mh; i++)
		{
			if (Fc-Bc+i > 0 && Fc-Bc+i < Mn)
			{
				s[i+Fc-Bc] += sband[i] * filter[i];
				s[*samplecount-(i+Fc-Bc)] += sband[sbsize-i] * filter[i];
			}
		}
	}

	printf("\n");

	fft(s, s, *samplecount, 1);
	*samplecount = roundoff(Xsize/pixpersec);

	normi(&s, *samplecount, 1, 1.0);

	return s;
}
