#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <fftw3.h>
#include <float.h>
#include <time.h>
#include <string.h>

#include "util.h"
#include "image_io.h"
#include "sound_io.h"
#include "dsp.h"

char *version = "v1.2 build7";
char *date = "Q2 2013";

#define MSG_NUMBER_EXPECTED "A number is expected after %s\nExiting with error.\n"

#ifdef QUIET
quiet=1;
#else
quiet=0;
#endif

int32_t mode;

void settingsinput(int32_t *bands, int32_t samplecount, int32_t *samplerate, double *basefreq, double maxfreq, double *pixpersec, double *bandsperoctave, int32_t Xsize, int32_t mode)
{
	int32_t i;
	double gf, f, trash;
	double ma;
	FILE *freqcfg;
	char byte;
	int32_t unset=0, set_min=0, set_max=0, set_bpo=0, set_y=0;
	int32_t set_pps=0, set_x=0;
	size_t filesize;
	char conf_path[FILENAME_MAX];

	sprintf(conf_path, "%s/%s", getenv("HOME"), ".mde.conf");
	freqcfg=fopen(conf_path, "rb");

	if (*samplerate==0)
	{
		if (quiet==1)
		{
			fprintf(stderr, "Please provide a sample rate for your output sound.\nUse --sample-rate (-r).\nExiting with error.\n");
			exit(EXIT_FAILURE);
		}
		
		printf("Sample rate [44100] : ");
		*samplerate=getfloat();
		if (*samplerate==0 || *samplerate<-2147483647)
			*samplerate = 44100;
	}

	if (*basefreq!=0)	set_min=1;
	if (maxfreq!=0)		set_max=1;
	if (*bandsperoctave!=0)	set_bpo=1;
	if (*bands!=0)		set_y=1;
	unset = set_min + set_max + set_bpo + set_y;

	if (unset==4)
	{
		if (mode==0)
			fprintf(stderr, "You have set one parameter too many.\nUnset either --min-freq (-min), --max-freq (-max), --bpo (-b)\nExiting with error.\n");
		if (mode==1)
			fprintf(stderr, "You have set one parameter too many.\nUnset either --min-freq (-min), --max-freq (-max), --bpo (-b) or --height (-y)\nExiting with error.\n");
		exit(EXIT_FAILURE);
	}

	if (*pixpersec!=0)	set_pps=1;
	if (Xsize!=0)		set_x=1;

	if (set_x+set_pps==2 && mode==0)
	{
		fprintf(stderr, "You cannot define both the image width and the horizontal resolution.\nUnset either --pps (-p) or --width (-x)\nExiting with error.\n");
		exit(EXIT_FAILURE);
	}

	if (freqcfg)
	{
		for (i=0; i<(int32_t) (4*sizeof(double)); i++)
			filesize=fread(&byte, sizeof(char), 1, freqcfg);
		rewind(freqcfg);
	}
	if (filesize==1)
	{
		if (*basefreq==0)	fread(basefreq, sizeof(double), 1, freqcfg);
		else			fread(&trash, sizeof(double), 1, freqcfg);
		if (maxfreq==0)		fread(&maxfreq, sizeof(double), 1, freqcfg);
		else			fread(&trash, sizeof(double), 1, freqcfg);
		if (*bandsperoctave==0)	fread(bandsperoctave, sizeof(double), 1, freqcfg);
		else			fread(&trash, sizeof(double), 1, freqcfg);
		if (*pixpersec==0)	fread(pixpersec, sizeof(double), 1, freqcfg);
		else			fread(&trash, sizeof(double), 1, freqcfg);
	}
	else
	{
		if (*basefreq==0)	*basefreq=27.5;
		if (maxfreq==0)		maxfreq=20000;
		if (*bandsperoctave==0)	*bandsperoctave=12;
		if (*pixpersec==0)	*pixpersec=150;
	}
	if (freqcfg)
		fclose(freqcfg);

	if (unset<3 && set_min==0)
	{
		if (quiet==1)
		{
			fprintf(stderr, "Please define a minimum frequency.\nUse --min-freq (-min).\nExiting with error.\n");
			exit(EXIT_FAILURE);
		}
		printf("Min. frequency (Hz) [%.3f]: ", *basefreq);
		gf=getfloat();
		if (gf != 0)
			*basefreq=gf;
		unset++;
		set_min=1;
	}
	*basefreq /= *samplerate;

	if (unset<3 && set_bpo==0)
	{
		if (quiet==1)
		{
			fprintf(stderr, "Please define a bands per octave setting.\nUse --bpo (-b).\nExiting with error.\n");
			exit(EXIT_FAILURE);
		}
		printf("Bands per octave [%.3f]: ", *bandsperoctave);
		gf=getfloat();
		if (gf != 0)
			*bandsperoctave=gf;
		unset++;
		set_bpo=1;
	}

	if (unset<3 && set_max==0)
	{
		i=0;
		do
		{
			i++;
			f=*basefreq * pow(LOGBASE, (i / *bandsperoctave));
		}
		while (f<0.5);
		
		ma=*basefreq * pow(LOGBASE, ((i-2) / *bandsperoctave)) * *samplerate;
		
	
		if (maxfreq > ma)
			if (fmod(ma, 1.0) == 0.0)
				maxfreq = ma;
			else
				maxfreq = ma - fmod(ma, 1.0);
	
		if (mode==0)
		{
			if (quiet==1)
			{
				fprintf(stderr, "Please define a maximum frequency.\nUse --max-freq (-max).\nExiting with error.\n");
				exit(EXIT_FAILURE);
			}
			printf("Max. frequency (Hz) (up to %.3f) [%.3f]: ", ma, maxfreq);
			gf=getfloat();
			if (gf != 0)
				maxfreq=gf;
		
			if (maxfreq > ma)
				if (fmod(ma, 1.0) == 0.0)
					maxfreq = ma;
				else
					maxfreq = ma - fmod(ma, 1.0);
		}
		
		unset++;
		set_max=1;
	}

	if (set_min==0)
	{
		*basefreq = pow(LOGBASE, (*bands-1) / *bandsperoctave) * maxfreq;
		printf("Min. frequency : %.3f Hz\n", *basefreq);
		*basefreq /= *samplerate;
	}

	if (set_max==0)
	{
		maxfreq = pow(LOGBASE, (*bands-1) / *bandsperoctave) * (*basefreq * *samplerate);
		printf("Max. frequency : %.3f Hz\n", maxfreq);
	}

	if (set_y==0)
	{
		*bands = 1 + roundoff(*bandsperoctave * (log_b(maxfreq) - log_b(*basefreq * *samplerate)));
		printf("Bands : %d\n", *bands);
	}

	if (set_bpo==0)
	{
		if (LOGBASE==1.0)
			*bandsperoctave = maxfreq / *samplerate;
		else
			*bandsperoctave = (*bands-1) / (log_b(maxfreq) - log_b(*basefreq * *samplerate));
		printf("Bands per octave : %.3f\n", *bandsperoctave);
	}

	if (set_x==1 && mode==0)
	{
		*pixpersec = (double) Xsize * (double) *samplerate / (double) samplecount;
		printf("Pixels per second : %.3f\n", *pixpersec);
	}

	if ((mode==0 && set_x==0 && set_pps==0) || (mode==1 && set_pps==0))
	{
		if (quiet==1)
		{
			fprintf(stderr, "Please define a pixels per second setting.\nUse --pps (-p).\nExiting with error.\n");
			exit(EXIT_FAILURE);
		}
		printf("Pixels per second [%.3f]: ", *pixpersec);
		gf=getfloat();
		if (gf != 0)
			*pixpersec=gf;
	}

	*basefreq *= *samplerate;

	freqcfg=fopen(conf_path, "wb");

	if (freqcfg==NULL)
	{
		fprintf(stderr, "Cannot write to configuration file");
		exit(EXIT_FAILURE);
	}
	fwrite(basefreq, sizeof(double), 1, freqcfg);
	fwrite(&maxfreq, sizeof(double), 1, freqcfg);
	fwrite(bandsperoctave, sizeof(double), 1, freqcfg);
	fwrite(pixpersec, sizeof(double), 1, freqcfg);
	fclose(freqcfg);

	*basefreq /= *samplerate;
	*pixpersec /= *samplerate;
}

void print_help()
{
	printf(
		"---Music Discovery Engine HELP---\n"
		"\n"
		"Usage: mde [options] input_file output_file [options]. Example:\n"
		"mde -q in.bmp out.wav --noise --min-freq 55 -max 16000 --pps 100 -r 44100 -f 16\n"
		"\n"
		"Options:\n"
		"--help, -h, /?                Display this help\n"
		"--version, -v                 Display the version of this program\n"
		"--quiet, -q                   No-prompt mode. Useful for scripting\n"
		"--analysis, -a                Analysis mode\n"
		"--sine, -s                    Sine synthesis mode\n"
		"--min-freq, -min [real]       Minimum frequency in Hertz\n"
		"--max-freq, -max [real]       Maximum frequency in Hertz\n"
		"--bpo, -b [real]              Frequency resolution in Bands Per Octave\n"
		"--pps, -p [real]              Time resolution in Pixels Per Second\n"
		"--height, -y [integer]        Specifies the desired height of the spectrogram\n"
		"--width, -x [integer]         Specifies the desired width of the spectrogram\n"
		"--sample-rate, -r [integer]   Sample rate of the output sound\n"
		"--format-param, -f [integer]  Output format option. This is bit-depth for WAV files (8/16/32, default: 32).\n"
		);
}

void check_mode()
{
	if (mode==0)
		do
		{
			if (quiet==1)
			{
				fprintf(stderr, "Please specify an operation mode.\nUse either --analysis (-a) or --sine (-s).\nExiting with error.\n");
				exit(EXIT_FAILURE);
			}
			printf("Choose mode (Press 1 or 2) :\n\t1. Analysis\n\t2. Sine synthesis\n> ");
			mode=getfloat();
		}
		while (mode!=1 && mode!=2);
}


int main(int argc, char *argv[])
{
	FILE *fin;
	FILE *fout;
	int32_t  i;
	double **sound, **image, basefreq=0, maxfreq=0, pixpersec=0, bpo=0;
	int32_t channels, samplecount=0, samplerate=0, Xsize=0, Ysize=0, format_param=0;
	int32_t clockb;
	char mode=0, *in_name=NULL, *out_name=NULL;

	pi=PI_D;
	LOGBASE=LOGBASE_D;
	LOOP_SIZE_SEC=LOOP_SIZE_SEC_D;
	BMSQ_LUT_SIZE=BMSQ_LUT_SIZE_D;

	printf("Music Discovery Engine %s\n", version);

	srand(time(NULL));

	for (i=1; i<argc; i++)
	{
		if (strcmp(argv[i], "/?")==0)
		{
			argv[i][0] = '-';
			argv[i][1] = 'h';
		}

		if (argv[i][0] != '-')
		{
			if (in_name==NULL)
				in_name = argv[i];
			else
				if (out_name==NULL)
					out_name = argv[i];
				else
				{
					fprintf(stderr, "You can only have two file names as parameters.\nRemove parameter \"%s\".\nExiting with error.\n", argv[i]);
					exit(EXIT_FAILURE);
				}
		}
		else
		{
			if (strcmp(argv[i], "--analysis")==0	|| strcmp(argv[i], "-a")==0)
				mode=1;

			if (strcmp(argv[i], "--sine")==0	|| strcmp(argv[i], "-s")==0)
				mode=2;

			if (strcmp(argv[i], "--quiet")==0	|| strcmp(argv[i], "-q")==0)
				quiet=1;

			if (strcmp(argv[i], "--sample-rate")==0	|| strcmp(argv[i], "-r")==0)
				if (str_isnumber(argv[++i]))
						samplerate = atoi(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--min-freq")==0	|| strcmp(argv[i], "-min")==0)
				if (str_isnumber(argv[++i]))
				{
					basefreq = atof(argv[i]);
					if (basefreq==0)
							basefreq = DBL_MIN;
				}
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--max-freq")==0	|| strcmp(argv[i], "-max")==0)
				if (str_isnumber(argv[++i]))
						maxfreq = atof(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--bpo")==0		|| strcmp(argv[i], "-b")==0)
				if (str_isnumber(argv[++i]))
						bpo = atof(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--pps")==0		|| strcmp(argv[i], "-p")==0)
				if (str_isnumber(argv[++i]))
						pixpersec = atof(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--height")==0	|| strcmp(argv[i], "-y")==0)
				if (str_isnumber(argv[++i]))
						Ysize = atoi(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--width")==0	|| strcmp(argv[i], "-x")==0)
				if (str_isnumber(argv[++i]))
						Xsize = atoi(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--format-param")==0	|| strcmp(argv[i], "-f")==0)
				if (str_isnumber(argv[++i]))
						format_param = atoi(argv[i]);
				else
				{
					fprintf(stderr, MSG_NUMBER_EXPECTED, argv[i-1]);
					exit(EXIT_FAILURE);
				}

			if (strcmp(argv[i], "--version")==0	|| strcmp(argv[i], "-v")==0)
			{
				printf("By Pradyumna Kamat and Vidur Kalra in %s\n", date);
				exit(EXIT_SUCCESS);
			}

			if (strcmp(argv[i], "--help")==0	|| strcmp(argv[i], "-h")==0)
			{
				print_help();
				exit(EXIT_SUCCESS);
			}

		}
	}

	if (in_name!=NULL)
	{
		fin=fopen(in_name, "rb");

		if (fin==NULL)
		{
			fprintf(stderr, "The input file %s could not be found\nExiting with error.\n", in_name);
			exit(EXIT_FAILURE);
		}
		check_mode();
		printf("Input file : %s\n", in_name);
	}
	else
	{
		if (quiet==1)
		{
			fprintf(stderr, "Please specify an input file.\nExiting with error.\n");
			exit(EXIT_FAILURE);
		}

		printf("Type 'help' to read the manual page\n");

		do
		{
			check_mode();
			printf("Input file : ");
			in_name=getstring();

			if (strcmp(in_name, "help")==0)
			{
				fin=NULL;
				print_help();
			}
			else
				fin=fopen(in_name, "rb");
		}
		while (fin==NULL);
	}

	if (out_name!=NULL)
	{
		fout=fopen(out_name, "wb");

		if (fout==NULL)
		{
			fprintf(stderr, "The output file %s could not be opened.\nPlease make sure it isn't opened by any other program and press Return.\nExiting with error.\n", out_name);
			exit(EXIT_FAILURE);
		}
		printf("Output file : %s\n", out_name);
	}
	else
	{
		if (quiet==1)
		{
			fprintf(stderr, "Please specify an output file.\nExiting with error.\n");
			exit(EXIT_FAILURE);
		}
		printf("Output file : ");
		out_name=getstring();
		fout=fopen(out_name, "wb");

		while (fout==NULL)
		{
			fprintf(stderr, "The output file %s could not be opened.\nPlease make sure it isn't opened by any other program and press Return.\n", out_name);
			getchar();
			fout=fopen(out_name, "wb");
		}
	}

	for (i=0; i<strlen(in_name); i++) if (in_name[i]>='A' && in_name[i]<='Z') in_name[i] -= 'A' - 'a';
	if (mode==0 && strstr(in_name, ".wav")!=NULL && strstr(in_name, ".wav")[4]==0)
		mode=1;
		
		for (i=0; i<strlen(in_name); i++) if (in_name[i]>='A' && in_name[i]<='Z') in_name[i] -= 'A' - 'a';
	if (mode==0 && strstr(in_name, ".bmp")!=NULL && strstr(in_name, ".bmp")[4]==0)
		mode=2;

		check_mode();

		
	if (mode==1)
	{
		sound=wav_in(fin, &channels, &samplecount, &samplerate);

		settingsinput(&Ysize, samplecount, &samplerate, &basefreq, maxfreq, &pixpersec, &bpo, Xsize, 0);
		image = anal(sound[0], samplecount, samplerate, &Xsize, Ysize, bpo, pixpersec, basefreq);
		
		bmp_out(fout, image, Ysize, Xsize);
	}
	if (mode==2)
	{
		sound = calloc (1, sizeof(double *));
		image = bmp_in(fin, &Ysize, &Xsize);

		if (format_param==0)
			if (quiet==0)
				format_param = wav_out_param();
			else
				format_param = 32;

		settingsinput(&Ysize, samplecount, &samplerate, &basefreq, maxfreq, &pixpersec, &bpo, Xsize, 1);

		sound[0] = synt_sine(image, Xsize, Ysize, &samplecount, samplerate, basefreq, pixpersec, bpo);

		wav_out(fout, sound, 1, samplecount, samplerate, format_param);
	}

	clockb=gettime();
	printf("Processing time : %.3f s\n", (double) (clockb-clocka)/1000.0); 

	return 0;
}
